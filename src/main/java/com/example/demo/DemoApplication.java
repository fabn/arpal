package com.example.demo;

import com.github.javafaker.Faker;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.PostConstruct;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@SpringBootApplication
public class DemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	}

}


@CrossOrigin("*") // quali frontend possono accedere al servizio * => tutti
@RestController
class Controller {
	@Autowired
	ProductRepository repository;

	@GetMapping("/")
	Map<String,Object> home() throws UnknownHostException {
		HashMap<String, Object> returnValue = new HashMap<>();
		returnValue.put("Hello", "From spring boot@" + InetAddress.getLocalHost().getHostName());
		return returnValue;
	}
	
	@GetMapping("/products")
	List<Product> getProducts() {
		return repository.findAll();
	}

	@GetMapping("/products/{id}")
	Product productById(@PathVariable Long id)  {
		return repository.findById(id).orElseThrow();
	}
}

@Entity
class Product {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	Long id;
	String name;

	String ean;

	String category;

	String price;

	public void setId(Long id) {
		this.id = id;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public String getEan() {
		return ean;
	}

	public void setEan(String ean) {
		this.ean = ean;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}
}

@Repository
interface ProductRepository extends JpaRepository<Product, Long> {

}

@Service
class Seed {

	private static final Faker faker = new Faker();
	private static final Logger logger = LoggerFactory.getLogger(Controller.class);

	@Autowired
	ProductRepository repository;

	@PostConstruct
	void createProducts() {
		logger.info("Collegato al database, trovati {} prodotti", repository.count());
		if (repository.count() > 0) {
			logger.info("Skipping database initialization");
			return;
		}
		logger.info("Creo i prodotti su database vuoto");
		for (int i = 0; i < 20; i++) {
			Product product = new Product();
			product.setName(faker.commerce().productName());
			product.setEan(faker.code().ean13());
			product.setCategory(faker.commerce().department());
			product.setPrice(faker.commerce().price(10, 99));
			repository.save(product);
		}
		logger.info("Database inizializzato con {} prodotti", repository.count());
	}
}

@Configuration
class RedisConfig {

	// Logger
	private static final Logger logger = LoggerFactory.getLogger(RedisConfig.class);

	// inject the actual template
	@Autowired
	private RedisTemplate<String, String> template;

	@PostConstruct
	public void listRedisKeys() {
		logger.info("Getting redis keys");
		for (String key : template.keys("*")) {
			logger.info("Redis key available {}", key);
		}
	}
}