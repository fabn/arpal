# Application manifests for k8s

Applicare tutti i manifest nell'ordine corretto usando il comando

```bash
kubectl apply -k k8s 
```

dove `k8s` è il nome della directory che contiene la kustomization


# Preview generazione manifesti

```bash
kubectl kustomize k8s
```


# Dev e prod

```bash
kubectl apply -k k8s/dev
kubectl apply -k k8s/prod
```
