import useSWR from 'swr'
import logo from './logo.svg';
import './App.css';

// Fetcher per useSWR
const fetcher = (...args) => fetch(...args).then(res => res.json())

function ProductList() {
    const { data: products, error } = useSWR('/products', fetcher)

    if (error) return <div>failed to load</div>
    if (!products) return <div>loading...</div>
    return <div className="productList">
        <ul>
            {products.map(p => <li key={p.id}>{p.name} - {p.ean}</li>)}
        </ul>
    </div>
}

function App() {
  return (
    <div className="App">
      <header className="App-header">
          <h1>Test</h1>
          <ProductList/>
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
      </header>
    </div>
  );
}

export default App;
